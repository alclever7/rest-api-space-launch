from marshmallow import Schema, fields, validate


class PlainCustomerSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    type = fields.Str(required=True)
    country = fields.Str(required=True)


class PlainRocketSchema(Schema):
    id = fields.Int(dump_only=True)
    vehicle_type = fields.Str(required=True)


class PlainFlightSchema(Schema):
    id = fields.Int(dump_only=True)
    flight_number = fields.Str(required=True)
    launch_date = fields.Date(required=True)
    launch_time = fields.Time(required=True)
    launch_site = fields.Str(required=True)

    mission_outcome = fields.Str(validate=validate.OneOf(["Success", "Failure"]), required=False)
    status = fields.Str(validate=validate.OneOf(["schedule", "count_down", "abort", "launch"]), required=False)
    landing_type = fields.Str(required=False)
    landing_outcome = fields.Str(required=False)

    rocket = fields.Nested(PlainRocketSchema(), dump_only=True)


class PlainCargoSchema(Schema):
    id = fields.Int(dump_only=True)
    payload_name = fields.Str(required=True)
    payload_type = fields.Str(required=False)
    payload_orbit = fields.Str(required=False)
    payload_mass = fields.Float(required=False)
    flight_id = fields.Int(required=True, load_only=True)


class CustomerSchema(PlainCustomerSchema):
    flights = fields.List(fields.Nested(PlainFlightSchema(), only=("id", "flight_number")), dump_only=True)


class EditFlightSchema(PlainFlightSchema):
    rocket_id = fields.Int(required=True, load_only=True)


class FlightSchema(EditFlightSchema):
    # cargos = fields.List(fields.Nested(PlainTagSchema()), dump_only=True)
    customers = fields.List(fields.Nested(PlainCustomerSchema()), dump_only=True)


class FlightSearchQueryArgsSchema(Schema):
    date = fields.Date(required=False, example="2023-02-08")
    date_from = fields.Date(required=False, example="2023-02-08")
    date_to = fields.Date(required=False, example="2023-02-08")
    customer_name = fields.Str(required=False, example="John")
    rocket_type = fields.Str(required=False, example="Falcon")


class RocketSchema(PlainRocketSchema):
    flights = fields.List(fields.Nested(PlainFlightSchema()), dump_only=True)


class CargoSchema(PlainCargoSchema):
    flight = fields.Nested(PlainFlightSchema(), dump_only=True, only=("id", "flight_number"))


class DeleteSchema(Schema):
    message = fields.Str()
