FROM python:3.10

EXPOSE 5000

WORKDIR /app

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN pip install flask

COPY . .

CMD ["flask", "run", "--host", "0.0.0.0"]