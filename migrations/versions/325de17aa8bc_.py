"""empty message

Revision ID: 325de17aa8bc
Revises: 40a4f6a76da7
Create Date: 2023-02-09 01:44:26.873755

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '325de17aa8bc'
down_revision = '40a4f6a76da7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('cargo', schema=None) as batch_op:
        batch_op.alter_column('payload_mass',
               existing_type=sa.REAL(),
               type_=sa.Float(precision=2),
               existing_nullable=True)

    with op.batch_alter_table('flight', schema=None) as batch_op:
        batch_op.add_column(sa.Column('status', sa.String(length=100), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('flight', schema=None) as batch_op:
        batch_op.drop_column('status')

    with op.batch_alter_table('cargo', schema=None) as batch_op:
        batch_op.alter_column('payload_mass',
               existing_type=sa.Float(precision=2),
               type_=sa.REAL(),
               existing_nullable=True)

    # ### end Alembic commands ###
