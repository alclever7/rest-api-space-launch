from db import db


class RocketModel(db.Model):
    __tablename__ = 'rocket'
    id = db.Column(db.Integer, primary_key=True)
    vehicle_type = db.Column(db.String(100), unique=True, nullable=False)

    flights = db.relationship("FlightModel", back_populates="rocket", lazy="dynamic")
