from db import db


class CustomerModel(db.Model):
    __tablename__ = 'customer'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    type = db.Column(db.String(200), nullable=False)
    country = db.Column(db.String(200), nullable=False)

    flights = db.relationship("FlightModel", back_populates="customers", secondary="flight_customer")
