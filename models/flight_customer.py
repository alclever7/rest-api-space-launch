from db import db


class FlightCustomerModel(db.Model):
    __tablename__ = 'flight_customer'
    id = db.Column(db.Integer, primary_key=True)

    flight_id = db.Column(db.Integer, db.ForeignKey("flight.id"), unique=False, nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey("customer.id"), unique=False, nullable=False)

