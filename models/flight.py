from db import db


class FlightModel(db.Model):
    __tablename__ = 'flight'
    id = db.Column(db.Integer, primary_key=True)
    flight_number = db.Column(db.String(25), nullable=False)
    launch_date = db.Column(db.Date(), nullable=False)
    launch_time = db.Column(db.Time(), nullable=False)
    launch_site = db.Column(db.String(100), nullable=False)
    mission_outcome = db.Column(db.String(100), nullable=True)
    failure_reason = db.Column(db.String(100), nullable=True)
    landing_type = db.Column(db.String(100), nullable=True)
    landing_outcome = db.Column(db.String(100), nullable=True)
    status = db.Column(db.String(100), nullable=True)

    rocket_id = db.Column(db.Integer, db.ForeignKey("rocket.id"), unique=False, nullable=False)
    rocket = db.relationship("RocketModel", back_populates="flights")

    cargos = db.relationship("CargoModel", back_populates="flight", lazy="dynamic")

    customers = db.relationship("CustomerModel", back_populates="flights", secondary="flight_customer")

