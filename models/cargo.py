from db import db


class CargoModel(db.Model):
    __tablename__ = 'cargo'
    id = db.Column(db.Integer, primary_key=True)
    payload_name = db.Column(db.String(250), nullable=False)
    payload_type = db.Column(db.String(200), nullable=True)
    payload_mass = db.Column(db.Float(precision=2), nullable=True)
    payload_orbit = db.Column(db.String(200), nullable=True)

    flight_id = db.Column(db.Integer, db.ForeignKey("flight.id"), unique=False, nullable=False)
    flight = db.relationship("FlightModel", back_populates="cargos")
