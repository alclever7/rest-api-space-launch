from flask_smorest import Blueprint, abort
from flask.views import MethodView
from models import CustomerModel, FlightModel
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from flask_jwt_extended import jwt_required
from schemas.mixed_schema import CustomerSchema, PlainCustomerSchema, DeleteSchema

from db import db

blp = Blueprint('Customers', __name__, 'Operation on customers')


@blp.route("/customers")
class CustomerList(MethodView):
    @jwt_required()
    @blp.response(200, PlainCustomerSchema(many=True))
    def get(self):
        return CustomerModel.query.all()

    @jwt_required()
    @blp.arguments(CustomerSchema)
    @blp.response(201, PlainCustomerSchema)
    def post(self, customer_data):
        customer = CustomerModel(**customer_data)
        try:
            db.session.add(customer)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return customer, 201


@blp.route("/customers/<string:customer_id>")
class Customer(MethodView):
    @jwt_required()
    @blp.response(200, CustomerSchema)
    def get(self, customer_id):
        customer = CustomerModel.query.get_or_404(customer_id)
        return customer

    @jwt_required()
    @blp.arguments(CustomerSchema)
    @blp.response(200, PlainCustomerSchema)
    def put(self, customer_data, customer_id):
        customer = CustomerModel.query.get_or_404(customer_id)
        customer.name = customer_data['name']
        customer.type = customer_data['type']
        customer.country = customer_data['country']
        try:
            db.session.add(customer)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return customer

    @jwt_required()
    @blp.response(200, DeleteSchema)
    def delete(self, customer_id):
        customer = CustomerModel.query.get_or_404(customer_id)
        db.session.delete(customer)
        db.session.commit()
        return {"message": "Customer deleted"}


@blp.route("/customers/<string:customer_id>/flights/<string:flight_id>")
class LinkFlightToCustomer(MethodView):
    @jwt_required()
    @blp.response(201, CustomerSchema)
    def post(self, customer_id, flight_id):
        customer = CustomerModel.query.get_or_404(customer_id)
        flight = FlightModel.query.get_or_404(flight_id)

        customer.flights.append(flight)

        try:
            db.session.add(customer)
            db.session.commit()
        except SQLAlchemyError as e:
            abort(500, message=str(e))
        return customer

    @jwt_required()
    @blp.response(200, DeleteSchema)
    def delete(self, customer_id, flight_id):
        customer = CustomerModel.query.get_or_404(customer_id)
        flight = FlightModel.query.get_or_404(flight_id)

        customer.flights.remove(flight)

        try:
            db.session.add(customer)
            db.session.commit()
        except SQLAlchemyError as e:
            abort(500, message=str(e))
        return {"message": "Flight removed from a Customer"}
