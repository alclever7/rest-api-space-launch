from flask_smorest import Blueprint, abort
from flask.views import MethodView
from models import FlightModel, CargoModel
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from flask_jwt_extended import jwt_required
from schemas.mixed_schema import CargoSchema, DeleteSchema, PlainCargoSchema

from db import db

blp = Blueprint('Cargos', __name__, 'Operation on cargos')


@blp.route("/cargos")
class CargoList(MethodView):
    @jwt_required()
    @blp.response(200, PlainCargoSchema(many=True))
    def get(self):
        return CargoModel.query.all()

    @jwt_required()
    @blp.arguments(PlainCargoSchema)
    @blp.response(201, CargoSchema)
    def post(self, cargo_data):
        flight_id = cargo_data['flight_id']
        flight = FlightModel.query.get(flight_id)
        if not flight:
            abort(400, message='Bad request. Ensure that rocket with provided "flight_id" existed.')

        cargo = CargoModel(**cargo_data)

        try:
            db.session.add(cargo)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return cargo


@blp.route("/cargos/<string:cargo_id>")
class Cargo(MethodView):
    @jwt_required()
    @blp.response(200, CargoSchema)
    def get(self, cargo_id):
        cargo = CargoModel.query.get_or_404(cargo_id)
        return cargo

    @jwt_required()
    @blp.arguments(PlainCargoSchema)
    @blp.response(200, CargoSchema)
    def put(self, cargo_data,  cargo_id):
        cargo = CargoModel.query.get_or_404(cargo_id)
        flight_id = cargo_data['flight_id']
        flight = FlightModel.query.get(flight_id)
        if not flight:
            abort(400, message='Bad request. Ensure that rocket with provided "flight_id" existed.')

        cargo.flight = flight
        cargo.payload_name = cargo_data['payload_name']

        if 'payload_type' in cargo_data:
            cargo.payload_type = cargo_data['payload_type']
        if 'payload_orbit' in cargo_data:
            cargo.payload_orbit = cargo_data['payload_orbit']
        if 'payload_mass' in cargo_data:
            cargo.payload_mass = cargo_data['payload_mass']

        try:
            db.session.add(cargo)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return cargo

    @jwt_required()
    @blp.response(200, DeleteSchema)
    def delete(self, cargo_id):
        cargo = CargoModel.query.get_or_404(cargo_id)
        db.session.delete(cargo)
        db.session.commit()
        return {"message": "Cargo deleted"}
