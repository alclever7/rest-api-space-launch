from flask_smorest import Blueprint, abort
from flask.views import MethodView
from models import RocketModel, CustomerModel
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from flask_jwt_extended import jwt_required
from schemas.mixed_schema import RocketSchema, PlainRocketSchema

from db import db

blp = Blueprint('Rockets', __name__, 'Operation on rockets')


@blp.route("/rockets")
class RocketList(MethodView):
    @jwt_required()
    @blp.response(200, PlainRocketSchema(many=True))
    def get(self):
        return RocketModel.query.all()

    @jwt_required()
    @blp.arguments(PlainRocketSchema)
    @blp.response(201, PlainRocketSchema)
    def post(self, rocket_data):
        rocket = RocketModel(**rocket_data)
        try:
            db.session.add(rocket)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return rocket


@blp.route("/rockets/<string:rocket_id>")
class Rocket(MethodView):
    @jwt_required()
    @blp.response(200, RocketSchema)
    def get(self, rocket_id):
        customer = RocketModel.query.get_or_404(rocket_id)
        return customer

    @jwt_required()
    @blp.arguments(PlainRocketSchema)
    @blp.response(200, PlainRocketSchema)
    def put(self, rocket_data,  rocket_id):
        rocket = CustomerModel.query.get_or_404(rocket_id)
        rocket.vehicle_type = rocket_data['vehicle_type']
        try:
            db.session.add(rocket)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return rocket

    @jwt_required()
    @blp.response(204)
    def delete(self, rocket_id):
        rocket = RocketModel.query.get_or_404(rocket_id)
        db.session.delete(rocket)
        db.session.commit()
        return {"message": "Rocket deleted"}, 204
