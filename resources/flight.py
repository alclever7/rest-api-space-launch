from flask_smorest import Blueprint, abort
from flask.views import MethodView
from models import FlightModel, RocketModel, CustomerModel
from sqlalchemy.exc import SQLAlchemyError, IntegrityError
from flask_jwt_extended import jwt_required
from schemas.mixed_schema import FlightSchema, PlainFlightSchema, EditFlightSchema, DeleteSchema, \
    FlightSearchQueryArgsSchema

from db import db

blp = Blueprint('Flights', __name__, 'Operation on flights')


@blp.route("/flights")
class FlightsList(MethodView):
    @jwt_required()
    @blp.arguments(FlightSearchQueryArgsSchema, location="query")
    @blp.response(200, FlightSchema(many=True))
    @blp.paginate()
    def get(self, flight_query_data, pagination_parameters):
        stmt = db.select(FlightModel)
        if 'customer_name' in flight_query_data:
            stmt = stmt.join(FlightModel.customers).where(
                CustomerModel.name.like(f'%{flight_query_data["customer_name"]}%'))
        if 'rocket_type' in flight_query_data:
            stmt = stmt.join(FlightModel.rocket).where(
                RocketModel.vehicle_type.like(f'%{flight_query_data["rocket_type"]}%'))
        if 'date' in flight_query_data:
            stmt = stmt.where(FlightModel.launch_date == flight_query_data['date'])
        if 'date_from' in flight_query_data:
            stmt = stmt.where(FlightModel.launch_date >= flight_query_data['date_from'])
        if 'date_to' in flight_query_data:
            stmt = stmt.where(FlightModel.launch_date <= flight_query_data['date_to'])
        return db.paginate(stmt, page=pagination_parameters.page, per_page=pagination_parameters.page_size)

    @jwt_required()
    @blp.arguments(EditFlightSchema)
    @blp.response(201, PlainFlightSchema)
    def post(self, flight_data):
        rocket_id = flight_data['rocket_id']
        rocket = RocketModel.query.get(rocket_id)
        if not rocket:
            abort(400, message='Bad request. Ensure that rocket with provided "rocket_id" existed.')
        flight = FlightModel(**flight_data)
        try:
            db.session.add(flight)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return flight


@blp.route("/flights/<string:flight_id>")
class Flight(MethodView):
    @jwt_required()
    @blp.response(200, FlightSchema)
    def get(self, flight_id):
        flight = FlightModel.query.get_or_404(flight_id)
        return flight

    @jwt_required()
    @blp.arguments(EditFlightSchema)
    @blp.response(200, EditFlightSchema)
    def put(self, flight_data, flight_id):
        flight = CustomerModel.query.get_or_404(flight_id)
        rocket_id = flight_data['rocket_id']
        rocket = RocketModel.query.get(rocket_id)
        if not rocket:
            abort(400, message='Bad request. Ensure that rocket with provided "rocket_id" existed.')

        flight.rocket = rocket
        flight.flight_number = flight_data['flight_number']
        flight.launch_date = flight_data['launch_date']
        flight.launch_time = flight_data['launch_time']
        flight.launch_site = flight_data['launch_site']

        if 'mission_outcome' in flight_data:
            flight.mission_outcome = flight_data['mission_outcome']
        if 'landing_type' in flight_data:
            flight.landing_type = flight_data['landing_type']
        if 'landing_outcome' in flight_data:
            flight.landing_outcome = flight_data['landing_outcome']

        try:
            db.session.add(flight)
            db.session.commit()
        except IntegrityError as ie:
            abort(400, message=str(ie))
        except SQLAlchemyError as ae:
            abort(500, message=str(ae))
        return flight

    @jwt_required()
    @blp.response(200, DeleteSchema)
    def delete(self, flight_id):
        flight = CustomerModel.query.get_or_404(flight_id)
        db.session.delete(flight)
        db.session.commit()
        return {"message": "Flight deleted"}
